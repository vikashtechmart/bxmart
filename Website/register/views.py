from django.shortcuts import render,redirect,HttpResponse
from django.contrib.auth.models import User,auth
from django.contrib import messages






# Create your views here.
# def signup(request):
#     if request.method=='POST':
#         first_name = request.POST['fname']
#         last_name = request.POST['lname']
#         username = request.POST['username']
#         password1 = request.POST['password_one']
#         password2 = request.POST['password_two']
#         email =request.POST['email']
#         contact =request.POST['contact']
#         address =request.POST['address']
#         if password1==password2:
#             if User.objects.filter(username=username).exists():
#                 print('username exists')
#                 messages.info(request,'Username already exist!')
#                 return render(request,'signup.html')
#             elif User.objects.filter(email=email).exists():
#                 print('email exists')
#                 messages.info(request, 'Email already exist!')
#                 return render(request, 'signup.html')
#             else:
#                 user= User.objects.create_user(username=username,password=password1,first_name=first_name,last_name=last_name,email=email)
#                 user.save()
#
#
#                 reg=Register_table(user_id=user,contact=contact,address=address)
#                 reg.save()
#                 print('user created!')
#                 return redirect('/')
#     return render(request,'signup.html')


def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        print(username,password)
        user =auth.authenticate(username=username,password=password)
        print(f"authenticating {user}")
        if user is not None:
            auth.login(request,user)
            print("user authenticated")
            return redirect('/')

        else:
            messages.info(request,'Incorrect Credentials!')
            return redirect('login')
    else:
         return render(request,'login.html')



def logout(request):
    auth.logout(request)
    return redirect("/")


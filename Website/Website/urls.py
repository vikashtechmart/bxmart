
from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/', include('register.urls')),
    path('',include('BudgetMart.urls')),
    path('paypal/', include('paypal.standard.ipn.urls')),
]
urlpatterns =urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
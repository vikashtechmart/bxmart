from django.contrib import admin
from .models import Items
from .models import AppDetail
from .models import Category
from .models import Cart
from .models import Register_table
from .models import Order_detail,Order_Receipt,Invoice,Setting

# Register your models here.


admin.site.register(Items)
admin.site.register(AppDetail)
admin.site.register(Category)
admin.site.register(Cart)
admin.site.register(Register_table)
admin.site.register(Order_detail)
admin.site.register(Order_Receipt)
admin.site.register(Invoice)
admin.site.register(Setting)



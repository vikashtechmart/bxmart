from django.urls import path
from . import views

urlpatterns = [
    path('', views.index,name='home'),
    path('<int:pid>/', views.prod_details,name='prod_details'),
    path('cart', views.cart,name='cart'),
    path('removeitems', views.removeitems,name='removeitems'),
    path('dashboard', views.dashboard, name='dashboard'),

    path('user_dashboard', views.user_dashboard, name='user_dashboard'),
    path('seller_dashboard', views.seller_dashboard, name='seller_dashboard'),

    path('updatecartqty', views.updatecartqty, name='updatecartqty'),
    path('get_cart_data', views.get_cart_data, name='get_cart_data'),
    path('register_user', views.register_user, name='register_user'),
    path('user_profile', views.user_profile, name='user_profile'),
    path('seller_user_profile', views.seller_user_profile, name='seller_user_profile'),

    path('edit_profile', views.edit_profile, name='edit_profile'),
    path('seller_edit_profile', views.seller_edit_profile, name='seller_edit_profile'),

    path('change_password', views.change_password, name='change_password'),
    path('seller_change_password', views.seller_change_password, name='seller_change_password'),

    path('seller_my_product', views.seller_my_product, name='seller_my_product'),
    path('my_product', views.my_product, name='my_product'),

    path('seller_add_product', views.seller_add_product, name='seller_add_product'),

    path('process_payment', views.process_payment,name='process_payment'),
    path('buy_process_payment', views.buy_process_payment, name='buy_process_payment'),
    path('payment_done', views.payment_done,name='payment_done'),
    path('payment_cancelled', views.payment_cancelled,name='payment_cancelled'),
    path('forget_password',views.forget_password,name='forget_password'),
    path('reset_password', views.reset_password, name='reset_password'),
    path('delete_product', views.delete_product, name='delete_product'),
    path('order_history', views.order_history, name='order_history'),
    path('seller_order_history', views.seller_order_history, name='seller_order_history'),
    path('getItem', views.getItem, name='getItem'),
    path('place_order', views.place_order, name='place_order'),
    path('order_done', views.order_done, name='order_done'),
    path('buy_order_done', views.buy_order_done, name='buy_order_done'),
    path('order_receipt', views.order_receipt, name='order_receipt'),
    path('buy_item', views.buy_item, name='buy_item'),
    path('add_item', views.add_item, name='add_item'),
    path('update_delivery', views.update_delivery, name='update_delivery'),
    path('invoice/<int:id>/', views.invoice, name='invoice'),
    path('seller_setting', views.seller_setting, name='seller_setting'),
    path('seller_support', views.seller_support, name='seller_support'),
    path('delItem', views.delItem, name='delItem'),
    path('payment_verify', views.payment_verify, name='payment_verify'),
    path('leave_cmnt', views.leave_cmnt, name='leave_cmnt'),

]
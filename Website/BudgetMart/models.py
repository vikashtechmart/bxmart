
from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Category(models.Model):
    category_name = models.CharField(max_length=100)
    category_desc= models.TextField()

class Items(models.Model):
    name = models.CharField(max_length=256)
    img = models.ImageField(upload_to='pics')
    s1_img =  models.ImageField(upload_to='pics',null=True,blank=True)
    s2_img = models.ImageField(upload_to='pics',null=True,blank=True)
    s3_img = models.ImageField(upload_to='pics',null=True,blank=True)
    s4_img = models.ImageField(upload_to='pics',null=True,blank=True)
    price = models.IntegerField()
    desc = models.TextField(default='you missed something')
    item_owner = models.ForeignKey(User,on_delete=models.CASCADE,blank=True,null=True)
    offer = models.BooleanField()
    date = models.DateField(auto_now_add=True,null=True)
    category_type= models.ForeignKey(Category,on_delete=models.CASCADE)


class AppDetail(models.Model):
    app_name= models.CharField(max_length=100)
    app_heading = models.TextField()
    c1_img = models.ImageField(upload_to='pics')
    c2_img = models.ImageField(upload_to='pics')
    c3_img = models.ImageField(upload_to='pics')
    desc = models.TextField()
    ship_charge=models.IntegerField(default=51)
    app_launch_date = models.DateTimeField(default=False)

class Cart(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    product = models.ForeignKey(Items,on_delete=models.CASCADE)
    qty = models.IntegerField()
    size=models.CharField(max_length=100,null=True,blank=True)
    status = models.BooleanField(default=False)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    updated_on = models.DateTimeField(auto_now_add=True,null=True)

class Register_table(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact = models.IntegerField()
    address =models.TextField()
    state = models.CharField(max_length=100,default='Bihar')
    district = models.CharField(max_length=200,default='Patna')
    pincode =models.IntegerField(default=800012)
    image=models.ImageField(upload_to='profiles/%y/%m/%d',blank=True,null=True)
    def __str__(self):
        return self.user.username

class Order_detail(models.Model):
    product=models.CharField(max_length=100)
    qty=models.CharField(max_length=100)


class Order(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    product_id=models.CharField(max_length=250,null=True,blank=True)
    product_qty=models.CharField(max_length=100)
    card_id=models.CharField(max_length=250)
    invoice=models.CharField(max_length=250)
    payment_mode=models.CharField(max_length=100)
    status=models.BooleanField(default=False)
    onprocessedon=models.DateTimeField(auto_now_add=True)

class Order_Receipt(models.Model):
    product=models.ForeignKey(Items,on_delete=models.CASCADE)
    qty=models.CharField(max_length=250)
    size=models.CharField(max_length=150,null=True,blank=True)
    order=models.ForeignKey(Order,on_delete=models.CASCADE)
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    invoice=models.CharField(max_length=125,null=True,blank=True)
    payment_mode=models.CharField(max_length=125,null=True,blank=True)
    status=models.BooleanField(default=False)
    added_on = models.DateTimeField(auto_now_add=True,null=True)


class Invoice(models.Model):
    invoice_no = models.CharField(max_length = 500, default = 1, null = True, blank = True)

class Setting(models.Model):
    ship_amt=models.IntegerField(default=0)
    delivery_pincode=models.IntegerField(null=True,blank=True)
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    Sign=models.ImageField(upload_to='pics',null=True,blank=True)

class Comment(models.Model):
    comment =models.CharField(max_length=250)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    product = models.ForeignKey(Items,on_delete=models.CASCADE)

class Delivey_otp(models.Model):
        otp = models.IntegerField()
        Order = models.ForeignKey(Order,on_delete=models.CASCADE)









from django.shortcuts import render,redirect,get_object_or_404,reverse,get_list_or_404
from django.http import HttpResponse,JsonResponse
from .models import Items
from .models import Category,Cart,AppDetail,Register_table,Order,Order_detail,Order_Receipt,Invoice,Setting,Comment
from . import views
from django.contrib.auth.models import User,auth
import json
from django.contrib import messages
from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings
import random
from django.core.mail import EmailMessage,BadHeaderError
from django.core.paginator import Paginator
from django.db.models import Q
import datetime

def index(request):

        apps=None
        val=None
        data=Items.objects.all()
        for i in data:
                val=i.name
        print(val)
        app_details=AppDetail.objects.all()
        for i in app_details:
                print(i.app_name)
                apps=i
        category = Category.objects.all()
        cart= Cart.objects.filter(user_id__id=request.user.id)
        c_count=cart.count()
        print(c_count,'c_count')
        list = None
        categortId=request.GET.get('category')
        print(f"id {categortId}")
        if 'q' in request.GET:
            item=request.GET['q']
            print(item)
            list=Items.objects.all().filter(Q(name__icontains=item));
            paginator = Paginator(list, 6)
            page = request.GET.get('page')
            list = paginator.get_page(page)

            print("from if",list)

        elif categortId :
                list=getProductByID(categortId)
                print(list)
                paginator = Paginator(list, 6)
                page = request.GET.get('page')
                list = paginator.get_page(page)

        else:
                list=Items.objects.all()
                paginator = Paginator(list,6)
                page=request.GET.get('page')
                list=paginator.get_page(page)

        return render(request, 'index.html', {'list': list, 'category': category, 'count': c_count, 'apps': apps,'data':val})


def prod_details(request,pid):
        product= Items.objects.filter(id=pid)
        print(product)
        for p in product:
                pass
        comment=Comment.objects.filter(product=p)
        print(comment)
        cart=Cart.objects.filter(user_id__id=request.user.id)
        count=cart.count()
        return render(request,'product_detail.html',{'product':product,"count":count,'pid':pid,'cmnt':comment})


def getItem(request):
        item=None
        item=Items.objects.all()

        data=[]
        for i in item:
                data.append(i.name)
        output = []
        for x in data:
                if x not in output:
                        output.append(x)
        print(output)

        return JsonResponse({'data':output})


def cart(request):
        msg=None
        cart=Cart.objects.filter(user__id=request.user.id)
        count=cart.count()
        print(count)
        if request.method=="POST":
                if request.user.is_authenticated:
                        if  request.user.is_superuser :
                               print("not superuser")
                               msg='sorry'
                               return JsonResponse({'msg': msg, 'count': count})

                        else:
                                pid = request.POST['pid']
                                qty = request.POST['qty']
                                size = request.POST['size']
                                print(pid, qty, size)
                                is_exist = Cart.objects.filter(product__id=pid, user__id=request.user.id, status=False)
                                if is_exist:
                                        msg = 'exist'
                                else:
                                        product = Items.objects.get(id=pid)
                                        user = User.objects.get(id=request.user.id)
                                        c = Cart(user=user, product=product, qty=qty, size=size)
                                        c.save()
                                        msg = 'saved'
                                        cart = Cart.objects.filter(user_id=request.user.id)
                                        count = cart.count()
                                return JsonResponse({'msg': msg, 'count': count})

                else:
                        msg='login'
                        return JsonResponse({'msg': msg, 'count': count})
        else:
                products=Cart.objects.filter(user__id=request.user.id)
                return render(request,'cart.html',{"count":count,"product":products})


def getProductByID(category):


        if category:
                return Items.objects.filter(category_type=category)
        else:
                return Items.objects.all()


def removeitems(request):
        cid= request.GET['cid']
        print(cid,'cid')
        c_item= Cart.objects.filter(id=cid)
        print(c_item)
        c_item.delete()
        print(c_item,'deleted')
        return HttpResponse(1)


def user_dashboard(request):
        image=None
        item=Items.objects.filter(id=6)
        for i in item:
                image=i.img
                print(i.img)

        reg=Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image=j.image
                        print("exit")

        apps = None
        app_details = AppDetail.objects.all()
        for i in app_details:
                print(i.app_name)
                apps = i
        return render(request,'user_dashboard.html',{'apps':apps,'image':image})

def seller_dashboard(request):
        image=None
        item=Items.objects.filter(id=6)
        for i in item:
                image=i.img
                print(i.img)

        reg=Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image=j.image
                        print("exit")

        apps = None
        app_details = AppDetail.objects.all()
        for i in app_details:
                print(i.app_name)
                apps = i
        product=Items.objects.all()
        pcount=product.count()
        order=Order.objects.all()
        ocount=order.count()
        usr=User.objects.all()
        ucount=usr.count()
        return render(request,'seller_dashboard.html',{'apps':apps,'image':image,'pcount':pcount,'ocount':ocount,'ucount':ucount})


def updatecartqty(request):
        qty=request.GET.get('qty')
        cid = request.GET.get('cid')
        print(cid, 'cid')
        print(qty,'qty')
        cart =Cart.objects.filter(id=cid).update(qty=qty)

        return HttpResponse(1)


def get_cart_data(request):
        items=Cart.objects.filter(user__id=request.user.id,status=False)
        total,quantity=0,0
        for i in items:
                total+= i.product.price
                quantity+= i.qty
                res={"total":total,'quantity':quantity}
                return JsonResponse(res)


def register_user(request):
        if request.method == 'POST':
                print("files",request.FILES)
                first_name = request.POST['fname']
                last_name = request.POST['lname']
                username = request.POST['username']
                password1 = request.POST['password_one']
                password2 = request.POST['password_two']
                print("hello")
                email = request.POST['email']
                contact = request.POST['contact']
                address = request.POST['address']
                if password1 == password2:
                        if User.objects.filter(username=username).exists():
                                print('username exists')
                                messages.info(request, 'Username already exist!')
                                return render(request, 'signup.html')
                        elif User.objects.filter(email=email).exists():
                                print('email exists')
                                messages.info(request, 'Email already exist!')
                                return render(request, 'signup.html')
                        else:
                                user = User.objects.create_user(username=username, email=email,password=password1)
                                user.first_name=first_name
                                user.last_name=last_name
                                user.save()


                                reg = Register_table(user=user,contact=contact,address=address,state='Bihar',district='Patna',pincode='800012')
                                if 'image' in request.FILES:
                                        img=request.FILES["image"]
                                        reg.image=img
                                        print('saving register')
                                reg.save()
                                print('user created!')
                                return redirect('/')
        return render(request, 'signup.html')


def user_profile(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print(j.image)


        con=None
        addr=None
        print(request.user.username)
        reg =Register_table.objects.filter(user_id__username=request.user.username)
        print(reg)
        for r in reg:
                reg=r

        return render(request,'user_profile.html',{'reg':reg,'image':image})


def seller_user_profile(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print(j.image)


        con=None
        addr=None
        print(request.user.username)
        reg =Register_table.objects.filter(user_id__username=request.user.username)
        print(reg)
        for r in reg:
                reg=r

        return render(request,'seller_user_profile.html',{'reg':reg,'image':image})


def edit_profile(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print(j.image)

        if request.method =='POST':
                fn = request.POST['fname']
                ln = request.POST['lname']
                em = request.POST['email']
                cont = request.POST['contact']
                state = request.POST['state']
                district = request.POST['dist']
                pincode = request.POST['pin']


                addr = request.POST['address']

                usr = User.objects.get(id=request.user.id)
                usr.first_name = fn
                usr.last_name = ln
                usr.email = em
                usr.save()
                reg = Register_table.objects.filter(user_id__username=request.user.username)
                if reg:
                        for r in reg:
                                if 'image' in request.FILES:
                                        img = request.FILES["image"]

                                r.contact=cont
                                r.address=addr
                                r.state=state
                                r.district=district
                                r.pincode=pincode
                                r.image=img
                                r.save()
                        return redirect('/user_profile')

                else:
                        new_reg=Register_table(user=request.user,contact=cont,address=addr,state=state,district=district,pincode=pincode)
                        if 'image' in request.FILES:
                                img = request.FILES["image"]
                                new_reg.image = img
                        new_reg.save()
                        print("register table saved")
                return redirect('/user_profile')
        else:
                reg=Register_table.objects.filter(user_id__username=request.user.username)

                for r in reg:
                        reg=r
                        print(reg,"reg")


        return render(request,'edit_profile.html',{'reg':reg,'image':image})

def seller_edit_profile(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print(j.image)

        if request.method =='POST':
                fn = request.POST['fname']
                ln = request.POST['lname']
                em = request.POST['email']
                cont = request.POST['contact']
                state = request.POST['state']
                district = request.POST['dist']
                pincode = request.POST['pin']


                addr = request.POST['address']

                usr = User.objects.get(id=request.user.id)
                usr.first_name = fn
                usr.last_name = ln
                usr.email = em
                usr.save()
                reg = Register_table.objects.filter(user_id__username=request.user.username)
                if reg:
                        for r in reg:
                                if 'image' in request.FILES:
                                        img = request.FILES["image"]

                                r.contact=cont
                                r.address=addr
                                r.state=state
                                r.district=district
                                r.pincode=pincode
                                r.image=img
                                r.save()
                        return redirect('/user_profile')

                else:
                        new_reg=Register_table(user=request.user,contact=cont,address=addr,state=state,district=district,pincode=pincode)
                        if 'image' in request.FILES:
                                img = request.FILES["image"]
                                new_reg.image = img
                        new_reg.save()
                        print("register table saved")
                return redirect('/user_profile')
        else:
                reg=Register_table.objects.filter(user_id__username=request.user.username)

                for r in reg:
                        reg=r
                        print(reg,"reg")


        return render(request,'seller_edit_profile.html',{'reg':reg,'image':image})


def change_password(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print(j.image)
        messages2=""
        print("change password")
        if request.method=='POST':
                newpwd=request.POST['newpassword']
                pwd=request.POST['password']
                repwd=request.POST['repassword']
                if newpwd==repwd:
                        print("pwd eq ewpwd")
                        usr=User.objects.get(id=request.user.id)
                        check=usr.check_password(pwd)
                        if check:
                                usr.set_password(newpwd)
                                usr.save()
                                messages2="password changed successfully!"
                                print("successfully changed")
                                # return render(request,'change_password.html',{'msg2':messages2})
                        else:
                                messages2="Incorrect User Password!"
                else:
                        messages2="Passsword did not match!"
                        # return render(request,"change_password.html",{'msg2':"changed"})

                return render(request,'change_password.html',{'msg2':messages2})
        else:
                return render(request,'change_password.html',{'image':image})

def seller_change_password(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print(j.image)
        messages2=""
        print("change password")
        if request.method=='POST':
                newpwd=request.POST['newpassword']
                pwd=request.POST['password']
                repwd=request.POST['repassword']
                if newpwd==repwd:
                        print("pwd eq ewpwd")
                        usr=User.objects.get(id=request.user.id)
                        check=usr.check_password(pwd)
                        if check:
                                usr.set_password(newpwd)
                                usr.save()
                                messages2="password changed successfully!"
                                print("successfully changed")
                                # return render(request,'change_password.html',{'msg2':messages2})
                        else:
                                messages2="Incorrect User Password!"
                else:
                        messages2="Passsword did not match!"
                        # return render(request,"change_password.html",{'msg2':"changed"})

                return render(request,'seller_change_password.html',{'msg2':messages2})
        else:
                return render(request,'seller_change_password.html',{'image':image})


def my_product(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print(j.image)
        cart= Cart.objects.filter(user_id__id=request.user.id)
        for c in cart:
                print(c.product.name)
        return render(request,'my_product.html',{"cart":cart,'image':image})

def seller_my_product(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print(j.image)


        item= Items.objects.all().filter(item_owner=request.user.id)
        for c in item:
                print(c.name)
        return render(request,'seller_my_product.html',{"item":item,"image":image})


def seller_add_product(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print(j.image)

        return render(request,'seller_add_product.html',{'image':image})

def order_history(request):
        print("order")
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print("form order",j.image)
        context={}
        all_order=[]
        order=Order.objects.filter(user_id=request.user.id)
        print(order)
        for ord in order:
                print("order id",ord.id)
                products=[]
                for id in ord.product_id.split(",")[:-1]:
                         pro=Items.objects.all().filter(id=id)
                         products+=pro
                print("order inv",ord.invoice)
                print(products)
                p_order={"order_id":ord.id,
                                 "products":products,
                                 "invoice":ord.invoice,
                                 "status":ord.status,
                                 "date":ord.onprocessedon,
                                 "image":image

                }
                all_order.append(p_order)
                context['order']=all_order
        return render(request,'order_history.html',context)

def seller_order_history(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print(j.image)

        ord=None
        order=Order_Receipt.objects.all();
        print(order)
        for ord in order:
                ord
                print(ord)
        return render(request,'seller_order_history.html',{'image':image,'order':order})


def process_payment(request):
        item=Cart.objects.filter(user_id__id=request.user.id,status=False)
        products=""
        amt=0
        cart_ids=""
        amt2=0
        p_ids=""
        inv = generate_inv()
        invoice=Invoice(invoice_no=inv)
        invoice.save();
        for i in item:
                products+=str(i.product.name)+"\n"
                p_ids+=str(i.product.id)+","
                amt+=i.product.price
                amt2=float(amt* 0.01368)
                cart_ids+=str(i.id)+","

        paypal_dict = {
                'business': settings.PAYPAL_RECEIVER_EMAIL,
                'amount': amt2,
                'item_name': products,
                'invoice': inv,
                'currency_code': 'USD',
                'notify_url': 'http://{}{}'.format('127.0.0.1:8000',
                                                   reverse('paypal-ipn')),
                'return_url': 'http://{}{}'.format('127.0.0.1:8000',
                                                   reverse('payment_done')),
                'cancel_return': 'http://{}{}'.format('127.0.0.1:8000',
                                                      reverse('payment_cancelled')),
        }
        usr=User.objects.get(username=request.user.username)
        ord=Order(user_id=usr.id,product_id=p_ids,card_id=cart_ids,invoice=inv,payment_mode='Online')
        ord.save()

        for j in item:
                ord_rept=Order_Receipt(user=usr,order=ord,product=i.product,qty=i.qty,size=i.size,invoice=inv,payment_mode='Online')
                ord_rept.save()
        request.session['order_id']=ord.id
        form = PayPalPaymentsForm(initial=paypal_dict)
        return render(request, 'process_payment.html', { 'form': form})
def buy_process_payment(request):
        amt2=0
        p_ids=''
        inv = generate_inv()
        invoice=Invoice(invoice_no=inv)
        invoice.save();
        id=request.session['buy_ids']
        prod= Items.objects.filter(id=id)
        amt=request.session['buy_price']
        qty=request.session['buy_qty']
        size=request.session['buy_size']
        for i in prod:
                print('from product',i)
                products=(i.name)
                p_ids+=str(i.id)
                amt+=i.price
                amt2=float(amt* 0.01368)


        paypal_dict = {
                'business': settings.PAYPAL_RECEIVER_EMAIL,
                'amount': amt2,
                'item_name': products,
                'invoice': inv,
                'currency_code': 'USD',
                'notify_url': 'http://{}{}'.format('127.0.0.1:8000',
                                                   reverse('paypal-ipn')),
                'return_url': 'http://{}{}'.format('127.0.0.1:8000',
                                                   reverse('payment_done')),
                'cancel_return': 'http://{}{}'.format('127.0.0.1:8000',
                                                      reverse('payment_cancelled')),
        }
        usr=User.objects.get(username=request.user.username)
        ord=Order(user_id=usr.id,product_id=p_ids,invoice=inv,payment_mode='Online')
        ord.save()

        ord_rept=Order_Receipt(user=usr,order=ord,product=i,qty=qty,size=size,invoice=inv,payment_mode='Online')
        ord_rept.save()
        request.session['order_id']=ord.id
        form = PayPalPaymentsForm(initial=paypal_dict)
        return render(request, 'process_payment.html', { 'form': form})


def payment_done(request):
        if "order_id" in request.session:
                order_id=request.session['order_id']
                order=get_object_or_404(Order,id=order_id)
                order.status=True
                order.save()

                for c in order.card_id.split(',')[:-1]:
                        cart_obj=Cart.objects.filter(id=c)
                        for c in cart_obj:
                                 print(c)
                                 c.delete()

        return  render(request,'payment_done.html')


def payment_cancelled(request):
        return  render(request,'payment_cancelled.html')


def forget_password(request):
        if request.method=='POST':
                 un= request.POST['uname']
                 pwd = request.POST['password']
                 usr=get_object_or_404(User,username=un)
                 usr.set_password(pwd)
                 usr.save()
                 print("password reset")
                 return render(request,'forgotpass.html',{'status':'success'})
        else:

                return render(request,"forgotpass.html")


def reset_password(request):
        un=request.GET['username']
        try:
                usr=get_object_or_404(User,username=un)
                usr.email
                print(usr.email)
                otp=random.randint(1000,9999)
                msg="Dear {}\n {} is your one time password(OTP)\n do not share with others\n Thank & Regards \n Techsoftcrews".format(usr,otp)
                try:
                        em=EmailMessage("Account Verification",msg,to=[usr.email])
                        print("proceeding to send")
                        em.send()
                        print("sent")
                        return JsonResponse({'status': 'sent', 'email': usr.email,'otp':otp})

                except:

                         return JsonResponse({'status':'error','email':usr.email})
        except:
                return JsonResponse({'status':'failed'})


def send_email(request):
    subject = request.POST['sub']
    message = "Dear Sir\n your last bill is pending \n plz depoite soon\n Thanks"
    from_email = "vknasriganj@gmail.com"
    if subject and message and from_email:
        try:

            em=EmailMessage(subject, message, to=['vknasriganj@gmail.com'])
            em.send()
            return HttpResponse(1)
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        return HttpResponseRedirect('/contact/thanks/')
    else:
        # In reality we'd use a form class
        # to get proper validation errors.
        return HttpResponse('Make sure all fields are entered and valid.')


def delete_product(request):
        if request.method=='POST':
               pid= request.POST['pid']
               item= Items.objects.get(id=pid)
               print(item)
               item.delete()
        return HttpResponse(1)


def place_order(request):
        amt=0
        registter=None
        context={}
        usr=User.objects.all().filter(username=request.user.username)
        for u in usr:
                u
        reg=Register_table.objects.all().filter(user_id__id=request.user.id)
        for r in reg:
                register = r
        cart=Cart.objects.all().filter(user_id__id=request.user.id,status=False)
        for i in cart:
                amt+=i.product.price*i.qty
        print(amt)
        sett=Setting.objects.all()
        for s in sett:
                pass
        print("user Objects:",usr)
        print("reg Objects:",reg)
        print("cart Objects:",cart)

        return render(request,'place_order.html',{'usr':u,'reg':r,'total':amt,'ship':s.ship_amt})


def buy_item(request):
        if request.method=='POST':
                pid =  request.POST['pid']
                request.session['buy_ids']=pid
                product=Items.objects.filter(id=pid)
                for p in product:
                       amt= p.price
                print("from product",p.price)
                bQty=request.POST['qty']
                bSize=request.POST.get('size')
                print(pid,bQty,bSize)
                request.session['buy_price']=amt
                request.session['buy_qty']=bQty
                request.session['buy_size']=bSize
                return HttpResponse(1)
        amt=0
        registter=None
        usr=User.objects.all().filter(username=request.user.username)
        for u in usr:
                u
        reg=Register_table.objects.all().filter(user_id__id=request.user.id)
        for r in reg:
                register = r


        price=int(request.session['buy_price'])
        qty=int(request.session['buy_qty'])
        print(price,qty)
        amt=price*qty

        print("user Objects:",usr)
        print("reg Objects:",reg)
        print("cart Objects:",cart)

        return render(request,'buy_item.html',{'usr':u,'reg':r,'total':amt})


def order_done(request):

        item = Cart.objects.filter(user_id__id=request.user.id, status=False)
        products = ""
        amt = 0
        cart_ids = ""
        p_ids = ""
        p_qty=""
        inv = generate_inv()
        invoice=Invoice(invoice_no=inv)
        invoice.save()
        for i in item:
                products += str(i.product.name) + "\n"
                p_ids += str(i.product.id) + ","
                p_qty +=str(i.qty)+","
                amt += i.product.price
                cart_ids += str(i.id) + ","
        usr = User.objects.get(username=request.user.username)
        ord = Order(user_id=usr.id, product_id=p_ids, card_id=cart_ids, invoice=inv,payment_mode='COD',product_qty=p_qty)
        ord.save()

        for i in item:
                ord_detail=Order_Receipt(user=usr,order=ord,product=i.product,qty=i.qty,size=i.size,invoice=inv,payment_mode='COD')
                ord_detail.save()


        request.session['order_id'] = ord.id
        order_obj=Order.objects.all().filter(id=ord.id)
        for i in order_obj:
                ordobj=i
                for cid in ordobj.card_id.split(',')[:-1]:
                        card=Cart.objects.filter(id=cid)
                        card.delete()


        return redirect('order_receipt')


def buy_order_done(request):

        inv = generate_inv()
        invoice=Invoice(invoice_no=inv)
        invoice.save();

        p_ids=request.session['buy_ids']
        prod=Items.objects.filter(id=p_ids)
        for p in prod:
                p

        price=request.session['buy_price']
        qty=request.session['buy_qty']
        size=request.session['buy_size']
        usr = User.objects.get(username=request.user.username)
        ord = Order(user_id=usr.id, product_id=p_ids, invoice=inv,payment_mode='COD',product_qty=qty)
        ord.save()


        ord_detail=Order_Receipt(user=usr,order=ord,product=p,qty=qty,size=size,payment_mode='COD',invoice=inv)
        ord_detail.save()

        request.session['order_id'] = ord.id
        del request.session['buy_qty']
        del request.session['buy_price']
        del request.session['buy_size']

        return redirect('order_receipt')


def order_receipt(request):
        products = []
        ord=Order.objects.filter(id=request.session['order_id'])
        print(' order obj',ord)
        for i in ord:
                i

        order_recpt=Order_Receipt.objects.filter(order_id=request.session['order_id'])
        print('order recept',order_recpt)
        for j in order_recpt:
                j

        return render(request,'order_done.html',{'product':order_recpt,'order':i})

def generate_inv():
    last_invoice = Invoice.objects.all().order_by('id').last()
    if not last_invoice:
        return 'BAX0001'
    invoice_no = last_invoice.invoice_no
    invoice_int = int(invoice_no.split('BAX')[-1])
    width = 4
    new_invoice_int = invoice_int + 1
    formatted = (width - len(str(new_invoice_int))) * "0" + str(new_invoice_int)
    new_invoice_no = 'BAX' + str(formatted)
    return new_invoice_no

def dashboard(request):
        return render(request,'dashboard.html')

def add_item(request):
        print("add_item")
        if request.method=='POST':
                tImg=None
                s1Img=None
                s2Img=None
                s3Img=None
                s4Img=None
                print("Post Mehtod")
                name = request.POST['name']
                price = request.POST['price']
                desc = request.POST['desc']
                offer = request.POST.get('offer',False)
                cate = request.POST['cate']

                owner=request.user
                print(cate)
                category=Category.objects.filter(id=cate)
                print(category)
                for c in category:
                        print(c)
                if 'tImg' in request.FILES:
                        tImg = request.FILES["tImg"]
                if 's1Img' in request.FILES:
                        s1Img = request.FILES["s1Img"]
                if 's2Img' in request.FILES:
                        s2Img = request.FILES["s2Img"]
                if 's3Img' in request.FILES:
                        s3Img = request.FILES["s3Img"]
                if 's4Img' in request.FILES:
                        s4Img = request.FILES["s4Img"]

                new_item=Items(name=name,img=tImg,s1_img=s1Img,s2_img=s2Img,s3_img=s3Img,s4_img=s4Img,price=price,offer=offer,category_type=c,item_owner=owner,desc=desc)
                new_item.save()

                return redirect('/seller_my_product')

        else:
                return render(request,'index.html')


def update_delivery(request):

        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print(j.image)
        return render(request,'update_delivery.html',{'image':image})

def invoice(request,id):
        date = datetime.datetime.now().today()
        sett=Setting.objects.all()
        for s in sett:
                pass

        inv=None
        order=Order_Receipt.objects.filter(id=id)
        print(order)
        for o in order:
                inv=o.invoice
                print(o)
        reg=Register_table.objects.filter(user=o.user)
        print(reg)
        for r in reg:
                addr=r.address
                print(addr)
        return render(request,'Invoice.html',{"date":date,"inv":inv,"order":o,"order_rep":order,'reg':r,'sett':s})


def seller_setting(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print("form order", j.image)

        if request.method=='POST':
                shipping = request.POST['ship']
                dPin = request.POST['pin']
                if 'sign' in request.FILES:
                        sign=request.FILES['sign']

                sett=Setting(user=request.user,ship_amt=shipping,delivery_pincode=dPin,Sign=sign)
                sett.save()
                return HttpResponse(1)
        else:
                return render(request,'seller_setting.html',{'image':image})

def seller_support(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print("form order", j.image)

        return render(request,'seller_support.html',{'image':image})

def delItem(request):
        if request.method=='POST':
                id=request.POST['id']
        item=Items.objects.filter(id=id)
        for i in item:
                i.delete()
                return HttpResponse(1)

def payment_verify(request):
        image = None
        item = Items.objects.filter(id=6)
        for i in item:
                image = i.img
                print(i.img)

        reg = Register_table.objects.filter(user=request.user.id)
        print(reg)
        if reg:
                for j in reg:
                        image = j.image
                        print("form order", j.image)

        if request.method=='POST':
           id=request.POST['ord']
           order=Order.objects.filter(id=id)
           for o in order:
                   pid=o.product_id
                   prod=Items.objects.filter(id=pid)
                   for p in prod:
                           pass
                   print(prod)

                   print(pid)
           print(order)
           return  render(request,'update_delivery.html',{'order':o,'prod':p,'image':image})
        else:
                id=request.GET['id']
                order=Order.objects.filter(id=id).update(status=True)
                ord=Order.objects.filter(id=id)
                print("order",ord)
                for o in ord:
                        pid = o.product_id
                        prod = Items.objects.filter(id=pid)
                        for p in prod:
                                pass
                return render(request, 'update_delivery.html', {'order': o, 'prod': p,'image':image})


def leave_cmnt(request):
        if request.method=='POST':
                pid=request.POST['pid']
                product=Items.objects.filter(id=pid)
                for p in product:
                        pass
                username=request.POST['username']
                user= User.objects.filter(username=username)
                for u in user:
                        pass
                comment=request.POST['comment']
                print(pid,username,comment)
                cmnt = Comment(comment=comment,user=u,product=p)
                cmnt.save()
                return HttpResponse(1)
        else:
                pid=request.GET['pid']
                data=[]
                cmnt=Comment.objects.filter(product_id=pid)
                for comment in cmnt:
                        data+=comment.comment
                return JsonResponse({'result':data})
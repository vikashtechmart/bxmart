from django.apps import AppConfig


class BudgetmartConfig(AppConfig):
    name = 'BudgetMart'

from django import template

register = template.Library()

@register.filter(name='range')
def rangee(max):
    a=[]
    for i in range(1,max):
        a+=str(i),
    return a

@register.filter(name='price_total')
def price_total(product,cart):
    price=product.price
    qty=cart.qty
    return price*qty


@register.filter(name='gross_total')
def gross_total(cart):
    sum=0
    for p in cart:
        price=p.product.price
        qty=p.qty
        sum+=(price*qty)
    return sum

@register.filter(name='gross_amt')
def gross_amt(cart):
    sum=0
    for p in cart:
        price=p.product.price
        qty=p.qty
        sum+=(price*qty)
    return sum